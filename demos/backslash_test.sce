// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mprintf("Use Makematrix to test backslash.\n")

n = (3 : 5 : 100);
N = size(n,"*");
d = zeros(N,1);
for k = 1 : N
    A = makematrix_border ( n(k) );
	e = (1:n(k))';
	b = A * e;
	x = A\b;
	d(k) = floor(min(assert_computedigits ( x , e )));
	mprintf("n=%d, cond(A)=%e, d=%d\n",...
	    n(k),cond(A),d(k));
end

scf();
plot(n,d);
xtitle("Backslash: test on Border matrix","N","Digits");

//
// Load this script into the editor
//
filename = "backslash_test.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );

