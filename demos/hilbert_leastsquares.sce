// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mprintf("Use Hilbert matrix to perform\n")
mprintf("least squares interpolation.\n")
// Reference
// Computational Methods in Physics, PHYS 3437
// Dr Rob Thacker, Dept of Astronomy & Physics (MM-301C)
//
// Numerical Analysis, Doron Levy
// Department of Mathematics, Stanford University
// 3.3 Least-squares Approximations
//
// An Algorithm for Least-Squares Polynomial Approximation
// Michael D. Godfrey*
// Information Systems Lab, Stanford University
//
// Description
// Approximate f(x) = sin(pi*x) with x in [0,1].
// We compute a degree 3 polynomial
//
// p(x) = a(1) + a(2)*x + a(3)*x^2
//
// which minimizes the integral:
//
// integral_0^1 |p(x)-f(x)|^2 dx
//
// The coefficients a are the solution of
// a linear system of equations,
// where the matrix is the 3-by-3 Hilbert matrix,
// and the right hand side is:
//
// b(k) = integral_0^1 x^(k-1) f(x) dx
//      = integral_0^1 g(x) dx
//
// with
//
// g(x) = x^(k-1) f(x)
//
// It is possible, and easy, to use the inverse
// Hilbert matrix and to compute accurately it with
// the makematrix_invhilbert function.
// We do not do this here, for simplicity.
//
function y = f(x)
  y = sin(%pi*x)
endfunction
function y = g(x,k)
  y = x^(k-1) * f(x)
endfunction
b = zeros(3,1);
for k = 1 : 3
  b(k) = intg(0,1,list(g,k));
end
//
// The exact right hand side is
// b = [
// 2/%pi
// 1/%pi
// (%pi^2-4)/%pi^3
// ];
//
A = makematrix_hilbert(3);
a = A\b;
// The polynomial is
// p(x) = - 0.0504655 + 4.1225116 * x - 4.1225116 * x^2
//
// Compute values of the polynomial
xI = linspace(0,1,1000);
V = makematrix_vandermonde(xI,3);
yI = V * a;
// Compute values of the function
z = sin(%pi.*xI);
// Draw the points
scf();
plot(xI,z,"b-");
plot(xI,yI,"r-");
legend(["Exact F","Degree 3 Polynomial"]);
xtitle("Least squares function fitting","X","Y");

//
// Load this script into the editor
//
filename = "hilbert_leastsquares.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );

