// This help file was automatically generated using helpupdate
// PLEASE DO NOT EDIT
demopath = get_absolute_file_path("makematrix.dem.gateway.sce");
subdemolist = [
"Vandermonde : Interpolation", "vandermonde_interpolation.sce"
"Hilbert : Least Squares", "hilbert_leastsquares.sce"
"Backslash Test", "backslash_test.sce"
];
subdemolist(:,2) = demopath + subdemolist(:,2)
