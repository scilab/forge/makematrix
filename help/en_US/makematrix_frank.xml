<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from makematrix_frank.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="makematrix_frank" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>makematrix_frank</refname><refpurpose>Returns Frank matrix.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   A = makematrix_frank ( n )
   A = makematrix_frank ( n , k )

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>n :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, the size of the matrix to return</para></listitem></varlistentry>
   <varlistentry><term>k :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, available values are k=0 or k=1 (default k=0)</para></listitem></varlistentry>
   <varlistentry><term>A :</term>
      <listitem><para> a n-by-n matrix of doubles, the Frank matrix</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Returns the Frank matrix of order n.
It is upper Hessenberg with determinant 1.
The parameter k = 0 is the default.
If k = 1 the elements are reflected about the anti-diagonal (1,n)--(n,1).
   </para>
   <para>
The Frank matrix has all positive eigenvalues and they occur in reciprocal pairs
(so that 1 is an eigenvalue if n is odd).
The eigenvalues of the Frank matrix may be obtained in terms of the zeros of the
Hermite polynomials.
The floor(n/2) smallest eigenvalues of the Frank matrix are ill conditioned,
the more so for bigger n.
   </para>
   <para>
det(makematrix_frank(n)) is very different from 1 for large n.
See Frank (1958) and Wilkinson (1960) for discussions.
   </para>
   <para>
This version incorporates improvements suggested by W. Kahan.
   </para>
   <para>
Eberlein (1971) and Varah (1986) give details of the eigensystem, as does
Rutishauser (1966).
   </para>
   <para>
Original author : Nicolas Higham, Scilab port : Michael Baudin, 2011
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
A = makematrix_frank ( 5 )
A = makematrix_frank ( 5 , 1 )

// In the following example, we compute the determinant of the
// Frank matrix for various values of n.
// For values of n greater than 10, the determinant computed
// with the det function is different from 1.
// Notice that, because of the use of extended precision on
// some processors, the result may differ from machine to machine
// (but the order of magnitude stays the same).
det(makematrix_frank ( 5 )')
det(makematrix_frank ( 10 )')
det(makematrix_frank ( 20 )')
det(makematrix_frank ( 50 )')
det(makematrix_frank ( 100 )')

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011 - Michael Baudin</member>
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) 2008-2009 - INRIA - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>W.L. Frank, Computing eigenvalues of complex matrices by determinant evaluation and by methods of Danilewski and Wielandt, J. Soc. Indust. Appl. Math., 6 (1958), pp. 378-392 (see pp. 385, 388).</para>
   <para>Nicolas Higham, Accuracy and Stability of Numerical Algorithms, A Gallery of Test Matrices</para>
   <para>G.H. Golub and J.H. Wilkinson, Ill-conditioned eigensystems and the computation of the Jordan canonical form, SIAM Review, 18 (1976), pp. 578-619 (Section 13).</para>
   <para>H. Rutishauser, On test matrices, Programmation en Mathematiques Numeriques, Editions Centre Nat. Recherche Sci., Paris, 165, 1966, pp. 349-365.  Section 9.</para>
   <para>J.H. Wilkinson, Error analysis of floating-point computation, Numer. Math., 2 (1960), pp. 319-340 (Section 8).</para>
   <para>J.H. Wilkinson, The Algebraic Eigenvalue Problem, Oxford University Press, 1965 (pp. 92-93).</para>
   <para>P.J. Eberlein, A note on the matrices denoted by B_n, SIAM J. Appl. Math., 20 (1971), pp. 87-92.</para>
   <para>J.M. Varah, A generalization of the Frank matrix, SIAM J. Sci. Stat. Comput., 7 (1986), pp. 835-839.</para>
</refsection>
</refentry>
