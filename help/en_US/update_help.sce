// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and
// creating them again from the .sci with help_from_sci.

//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating makematrix\n");
helpdir = fullfile(cwd);
funmat = [
"makematrix_gallery"
"makematrix_border"
"makematrix_cauchy"
"makematrix_circul"
"makematrix_diagonali"
"makematrix_dingdong"
"makematrix_frank"
"makematrix_frankmin"
"makematrix_hadamard"
"makematrix_hankel"
"makematrix_hilbert"
"makematrix_invhilbert"
"makematrix_magic"
"makematrix_moler"
"makematrix_normal"
"makematrix_pascal"
"makematrix_rosser"
"makematrix_toeplitz"
"makematrix_vandermonde"
"makematrix_wilkinsonm"
"makematrix_wilkinsonp"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "makematrix";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
