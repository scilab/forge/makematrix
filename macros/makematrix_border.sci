// Copyright (C) 2011 - Michael Baudin
//  Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_border ( n )
    //   Returns the Border matrix
    //
    // Calling Sequence
    // A = makematrix_border ( n )
    //
    // Parameters
    //  n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    //  A : a n-by-n matrix of doubles, the Border matrix
    //
    // Description
    //   Returns the Border matrix of size n.
    //
	// For i=1,2,...,n and j=1,2,...,n, if i==j, then
    //   <literal>
    //   A(i,j) = 1
    //   </literal>
    //	
    //   else if i<>n and j==n, then
    //   <literal>
    //   A(i,j) = 2^(1-i)
    //   </literal>
    //	
    //   else if j<>n and i==n, then
    //   <literal>
    //   A(i,j) = 2^(1-j)
    //   </literal>
    //
    //   "The matrix has (n-2) eigenvalues at 1.
    //   Wilkinson (1965, pp 94-7) gives some
    //   discussion of this property."
    //
    // Examples
    //   A = makematrix_border ( 5 )
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  J.C. Nash, Compact Numerical Methods for Computers: Linear Algebra and Function Minimisation, second edition, Adam Hilger, Bristol, 1990 (Appendix 1).
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_border" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_border" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_border" , n , "n" , 1 , "constant" )
	//
    apifun_checkscalar ( "makematrix_border" , n , "n" , 1 )
	//
    apifun_checkflint ( "makematrix_border" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_border" , n , "n" , 1 , 1 )
    //
    A = eye(n,n)
	A(n,1:n-1) = 2.^(1-(1:n-1))
	A(1:n-1,n) = 2.^(1-(1:n-1)')
endfunction

