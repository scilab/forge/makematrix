// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_cauchy ( varargin )
    //   Returns the Cauchy matrix
    //
    // Calling Sequence
    //    A = makematrix_cauchy ( x )
    //    A = makematrix_cauchy ( x , y )
    //
    // Parameters
    //  x : a 1-by-n or n-by-1 matrix of doubles, the x input
    //  y : a 1-by-n or n-by-1 matrix of doubles, the y input (default y=x)
    //  A : a n-by-n matrix of doubles, the Cauchy matrix
    //
    // Description
    //   Returns the Cauchy matrix of size n with entries x, y
    //
	// For i=1,2,...,n and j=1,2,...,n, we have:
    //   <literal>
    //   A(i,j) = 1 ./ (x(i) + y(j))
    //   </literal>
    //
    // Examples
    // A = makematrix_cauchy ( 1:5 )
    // A = makematrix_cauchy ( 1:5 , 6:10 )
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin
    // Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  Nicolas Higham, Accuracy and Stability of Numerical Algorithms, A Gallery of Test Matrices
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_cauchy" , rhs , 1 : 2 )
    apifun_checklhs ( "makematrix_cauchy" , lhs , 1 : 1 )
    //
    x = varargin(1)
	y = apifun_argindefault ( varargin , 2 , x )
	//
    apifun_checktype ( "makematrix_cauchy" , x , "x" , 1 , "constant" )
    apifun_checktype ( "makematrix_cauchy" , y , "y" , 2 , "constant" )
	//
    apifun_checkvector ( "makematrix_cauchy" , x , "x" , 1 )
    apifun_checkvector ( "makematrix_cauchy" , y , "y" , 2 )
    //
    x = x(:)
    y = y(:)'
    nx = length(x);
    ny = length(y);
    if nx<> ny then
        error(sprintf("The lengths of input vectors x (length=%d) and y (length=%d) are not equal.",nx, ny))
    end
    x = x(:,ones(nx,1))
    y = y(ones(nx,1),:)
    A = 1 ./(x + y)
endfunction

