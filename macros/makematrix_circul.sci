// Copyright (C) 2011 - Michael Baudin
//  Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_circul ( x )
    //   Returns the circular matrix
    //
    // Calling Sequence
	// A = makematrix_circul ( x )
    //
    // Parameters
    //  x : a n-by-1 or 1-by-n matrix of doubles, the input vector
    //  A : a n-by-n matrix of doubles, the circular matrix
    //
    // Description
    // Returns the circular matrix of size n with entries x.
	//
	// A circulant matrix is a special kind of Toeplitz matrix
	// where each row vector is rotated one element to the right
	// relative to the preceding row vector.
    //
    // Examples
    //   A = makematrix_circul ( 1:5 )
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  http://en.wikipedia.org/wiki/Circulant_matrix
	//
	// See also
	// makematrix_toeplitz
	// makematrix_hankel
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_circul" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_circul" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_circul" , x , "x" , 1 , "constant" )
	//
    apifun_checkvector ( "makematrix_circul" , x , "x" , 1 )
    //
    x = x(:)'
    n = size(x,"*")
    ixlast = n
    imlast = 0
    for i = 1:n
        if ( imlast >= 1 ) then
            A(i,1:imlast) = x(ixlast+1:n)
        end
        A(i,imlast+1:n) = x(1:ixlast)
        imlast = imlast + 1
        ixlast = ixlast - 1
    end
endfunction

