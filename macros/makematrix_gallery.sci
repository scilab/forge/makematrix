// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_gallery ( varargin )
    //   Test matrices
    //
    // Calling Sequence
    //  A = makematrix_gallery(3)
    //  A = makematrix_gallery(4)
    //  A = makematrix_gallery(5)
    //  A = makematrix_gallery(8)
    //  A = makematrix_gallery("cauchy",x)
    //  A = makematrix_gallery("cauchy",x,y)
    //  A = makematrix_gallery("circul",x)
    //
    // Parameters
    //  x : a 1-by-n or n-by-1 matrix of doubles, the x input
    //  y : a 1-by-n or n-by-1 matrix of doubles, the y input
    //  A : a n-by-n matrix of doubles, the matrix
    //
    // Description
    // makematrix_gallery(3) is a badly conditioned 3-by-3 matrix.
    //
    // makematrix_gallery(4) is the Wilson matrix. It is symmetric positive definite, integer inverse.
    //
    // makematrix_gallery(5) is an interesting eigenvalue problem.
    //
    //  makematrix_gallery(8) is a ill-conditionned matrix.
	//  Exact eigenvalues from Westlake (1968), p.150 are:
	//
	// <screen>
	// a = sqrt(10405);
	// b = sqrt(26);
	// e = [-10*a,   0,   510-100*b,  1000,   1000,   510+100*b, 1020,   10*a]';
	// </screen>
    //
    //  makematrix_gallery("cauchy",x) is the Cauchy matrix, where y is set to x (see makematrix_cauchy).
    //
    //  makematrix_gallery("cauchy",x,y) is the Cauchy matrix (see makematrix_cauchy).
    //
    //  makematrix_gallery("circul",x) is the circular matrix (see makematrix_circul).
    //
    // Examples
    //  A = makematrix_gallery(3)
	//  cond(makematrix_gallery(3))
	//
    //  A = makematrix_gallery(4)
	//
    //  A = makematrix_gallery(5)
	//
    //  A = makematrix_gallery(8)
	//  cond(makematrix_gallery(8))
	//
	//  A = makematrix_gallery("cauchy" , 1:5 )
	//
	//  A = makematrix_gallery("circul" , 1:5 )
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //       J.R. Westlake, A Handbook of Numerical Matrix Inversion and Solution of Linear Equations, John Wiley, New York, 1968.
    //       J.H. Wilkinson, The Algebraic Eigenvalue Problem, Oxford University Press, 1965.
    //      "The Test Matrix Toolbox for Matlab (Version 3.0)", Nicolas Higham, Numerical Analysis Report No. 276, September 1995, Manchester Centre for Computational Mathematics, The University of Manchester
    //      "Algorithm 694: a collection of test matrices in MATLAB", Nicolas Higham, ACM Transactions on Mathematical Software, Volume 17 ,  Issue 3  (September 1991), Pages: 289 - 305
    //      "Higham's Test Matrices ", John Burkardt, 2005, http://people.sc.fsu.edu/~burkardt/m_src/test_matrix/test_matrix.html
    //       "A Handbook of Numerical Matrix Inversion and Solution of Linear Equations", J.R. Westlake, John Wiley, New York, 1968.
    //       "The Algebraic Eigenvalue Problem", J.H. Wilkinson, Oxford University Press, 1965.


	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_gallery" , rhs , 1 : 3 )
    apifun_checklhs ( "makematrix_gallery" , lhs , 1 : 1 )
    //
    matname = varargin(1);
    select matname
    case "cauchy" then
        x = varargin(2);
        if ( rhs == 2 ) then
            y = x;
        else
            y = varargin(3);
        end
        sx = size(x,"*");
        if ( sx == 1 ) then
            x = 1:x
        end
        sy = size(y,"*");
        if ( sy == 1 ) then
            y = 1:y
        end
        A = makematrix_cauchy ( x , y );
    case "circul" then
        x = varargin(2);
        A = makematrix_circul ( x );
    case 3 then
        A = gallery3 ()
    case 4 then
        A = gallery4 ()
    case 5 then
        A = gallery5 ()
    case 8 then
        A = gallery8 ()
    else
        errmsg = msprintf(gettext("%s: Unrecognized matrix name ''%s''."), "makematrix_gallery", matname)
        error(errmsg)
    end
endfunction

// gallery3 --
//   A badly conditioned 3-by-3 matrix.
function A = gallery3 ()
    A = [
    -149   -50  -154
    537   180   546
    -27    -9   -25
    ]
endfunction

// gallery5 --
//   An interesting eigenvalue problem.
function A = gallery5 ()
    A = [
    -9          11         -21          63        -252
    70         -69         141        -421        1684
    -575         575       -1149        3451      -13801
    3891       -3891        7782      -23345       93365
    1024       -1024        2048       -6144       24572
    ]
endfunction

// gallery4 --
//   The Wilson matrix.  Symmetric pos def, integer inverse.
function A = gallery4 ()
    A = [
    10     7     8     7
    7     5     6     5
    8     6    10     9
    7     5     9    10
    ]
endfunction

// gallery8 --
function A = gallery8 ()
    A = [
    611.  196. -192.  407.   -8.  -52.  -49.   29.
    196.  899.  113. -192.  -71.  -43.   -8.  -44.
    -192.  113.  899.  196.   61.   49.    8.   52.
    407. -192.  196.  611.    8.   44.   59.  -23.
    -8.  -71.   61.    8.  411. -599.  208.  208.
    -52.  -43.   49.   44. -599.  411.  208.  208.
    -49.   -8.    8.   59.  208.  208.   99. -911.
    29.  -44.   52.  -23.  208.  208. -911.   99.
    ]
endfunction

