// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_hadamard ( n )
    //   Returns the Hadamard matrix
    //
    // Calling Sequence
    // A = makematrix_hadamard ( n )
    //
    // Parameters
    //  n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    //  A : a n-by-n matrix of doubles, the Hadamard matrix
    //
    // Description
    //   Returns the Hadamard matrix of size n.
	//   This is a square matrix whose entries are either +1 or -1
	//   and whose rows are mutually orthogonal.
	//
    //   n is expected to be a power of 2.
	//
	//  The Hadamard matrix satisfies
	//  <literal>
	//  A*A' = n*eye(A)
	//  </literal>
	//
	//  The Hadamard matrix satisfies
	//  <literal>
	//  det(A) = n^(n/2)
	//  </literal>
	//
	// This implementation is recursive on n,
	// and generates log2(n) recursive calls.
    //
    // Examples
    //  A = makematrix_hadamard ( 1 )
    //  A = makematrix_hadamard ( 2 )
    //  A = makematrix_hadamard ( 4 )
	//
	//  // Check orthogonal property
	//  A * A'
	//
	// for i = 1:10
	// n = 2^i;
	// A=  makematrix_hadamard(n);
	// mprintf("n=%d, det(A)=%e, n^(n/2)=%e\n",..
    // n,det(A),n^(n/2))
	// end
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  http://en.wikipedia.org/wiki/Hadamard_matrix
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_hadamard" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_hadamard" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_hadamard" , n , "n" , 1 , "constant" )
	//
    apifun_checkscalar ( "makematrix_hadamard" , n , "n" , 1 )
	//
    apifun_checkflint ( "makematrix_hadamard" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_hadamard" , n , "n" , 1 , 1 )
    //
    pow2n = log2(n);
    if ( n - 2^pow2n <>0 ) then
        errmsg = msprintf(gettext("%s: n is expected to be a power of 2."), "makematrix_hadamard");
        error(errmsg)
    end
    if n==1 then
        A = ones(1,1)
    else
        m = int(n/2)
        b = makematrix_hadamard(m)
        A = zeros(n,n)
        A(1:m,1:m) = b(1:m,1:m)
        A(1:m,m+1:n) = b(1:m,1:m)
        A(m+1:n,1:m) = b(1:m,1:m)
        A(m+1:n,m+1:n) = -b(1:m,1:m)
    end
endfunction

