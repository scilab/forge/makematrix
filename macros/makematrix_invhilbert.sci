// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_invhilbert ( n )
    //   Returns the inverse of the Hilbert matrix
    //
    // Calling Sequence
    // A = makematrix_invhilbert ( n )
    //
    // Parameters
    //  n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    //  A : a n-by-n matrix of doubles, the inverse of the Hilbert matrix
    //
    // Description
    //   Returns the inverse of the Hilbert matrix of size n.
	//
    //  This is translated from Scilab fortran source code hilber.f
    //
    // Examples
    // A = makematrix_invhilbert ( 5 )
	//
	// // Check the inverse property
    // A = makematrix_invhilbert ( 5 )
    // B = makematrix_hilbert ( 5 )
	// A * B
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  http://en.wikipedia.org/wiki/Hilbert_matrix
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_invhilbert" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_invhilbert" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_invhilbert" , n , "n" , 1 , "constant" )
	//
    apifun_checkscalar ( "makematrix_invhilbert" , n , "n" , 1 )
	//
    apifun_checkflint ( "makematrix_invhilbert" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_invhilbert" , n , "n" , 1 , 1 )
    //
    A = zeros(n,n);
    p = n
    for i = 1: n
        if i <> 1 then
            p = ((n-i+1)*p*(n+i-1))/(i-1)**2
        end
        r = p * p
        A(i,i) = r/(2*i-1)
        for j = i+1 : n
            r = - (r /(j-1)**2) * ((n-j+1) * (n+j-1))
            A(i,j) = r/(i+j-1)
            A(j,i) = A(i,j)
        end
    end
endfunction

