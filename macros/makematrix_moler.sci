// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_moler ( n )
    //   Returns the Moler matrix
    //
    // Calling Sequence
    // A = makematrix_moler ( n )
    //
    // Parameters
    //  n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    //  A : a n-by-n matrix of doubles, integer values
    //
    // Description
    //   Returns the Moler matrix of size n
    //   Positive definite, one small eigenvalue.
    //
	//   If i==j, then
    //   <literal>
    //   A(i,j) = i
    //   </literal>
	//
	//   else
    //   <literal>
    //   A(i,j) = min(i,j)-2
    //   </literal>
    //
    // Examples
    //  A = makematrix_moler ( 5 )
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  J.C. Nash, Compact Numerical Methods for Computers: Linear Algebra and Function Minimisation, second edition, Adam Hilger, Bristol, 1990 (Appendix 1).
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_moler" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_moler" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_moler" , n , "n" , 1 , "constant" )
	//
    apifun_checkscalar ( "makematrix_moler" , n , "n" , 1 )
	//
    apifun_checkflint ( "makematrix_moler" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_moler" , n , "n" , 1 , 1 )
    //
    i = (1:n)'
    i = i(:,ones(n,1))
    j = (1:n)
    j = j(ones(n,1),:)
    A = min(i,j)-2
    // Set only the diagonal
    I = sub2ind(size(A),1:n,1:n)
    A(I) = 1:n
endfunction

