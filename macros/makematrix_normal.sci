// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_normal ( n )
    //   Returns a normal random matrix
    //
    // Calling Sequence
    // A = makematrix_normal ( n )
    //
    // Parameters
    //  n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    //  A : a n-by-n matrix of doubles
    //
    // Description
    //   Returns a random matrix of size n following normal law with
    //   average 0 and standard deviation 1
    //
    // Uses the "grand" function, which has better
    // statistical properties than "rand".
    //
    //  See Edelman's for properties of these matrices.
    //  A normal matrix is not typical...
    //  Random matrices from normal law are very well conditionned.
    //
    // Examples
    // A = makematrix_normal ( 5 )
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  Nicolas Higham, Accuracy and Stability of Numerical Algorithms, A Gallery of Test Matrices
    //
	
	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_normal" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_normal" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_normal" , n , "n" , 1 , "constant" )
	//
    apifun_checkscalar ( "makematrix_normal" , n , "n" , 1 )
	//
    apifun_checkflint ( "makematrix_normal" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_normal" , n , "n" , 1 , 1 )
    //
    A = grand(n,n,"nor",0.0,1.0)
endfunction

