// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_pascal ( varargin )
    //   Returns the Pascal matrix.
    //
    // Calling Sequence
    //   A = makematrix_pascal ( n )
    //   A = makematrix_pascal ( n , k )
    //
    // Parameters
    //   n : a matrix of floating point integers, must be positive
    //   k : a matrix of floating point integers, k=-1 returns the upper Pascal matrix, k=0 returns the symetric Pascal matrix, k=1 returns the lower triangular Pascal matrix, other values produce an error. Default k=0.
    //   A : a n-by-n matrix of floating point integers
    //
    // Description
    //   Returns the Pascal matrix.
    //
    //   The matrix U=makematrix_pascal(n,1) is so that U'*U=S where
    //   S=makematrix_pascal(n,0).
    //
    // Any optional argument equal to the empty matrix [] is replaced by its default value.
    //
    //   The performances of this algorithm have been optimized.
    //
    // Examples
    // makematrix_pascal(5) // symetric
    // makematrix_pascal(5,-1) // upper
    // makematrix_pascal(5,0) // symetric
    // makematrix_pascal(5,1) // lower
    //
    // // Check a famous identity
    // n = 5;
    // U = makematrix_pascal(n,-1)
    // S = makematrix_pascal(n,0)
    // L = makematrix_pascal(n,1)
    // L*U - S
    //
    // Authors
    //   Copyright (C) 2009 - 2010 - Michael Baudin
    //
    // Bibliography
    // http://bugzilla.scilab.org/show_bug.cgi?id=7670
    // http://en.wikipedia.org/wiki/Pascal_matrix


    [lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_pascal" , rhs , 1:2 )
    apifun_checklhs ( "makematrix_pascal" , lhs , 1 )
    //
    n = varargin(1)
    k = apifun_argindefault ( varargin , 2 , 0 )
    //
    apifun_checktype ( "makematrix_pascal" , n , "n" , 1 , "constant" )
    apifun_checktype ( "makematrix_pascal" , k , "k" , 2 , "constant" )
    //
    apifun_checkrange ( "makematrix_pascal" , n , "n" , 1 , 0 , %inf )
    apifun_checkoption ( "makematrix_pascal" , k , "k" , 2 , [-1 0 1] )
    //
    apifun_checkflint ( "makematrix_pascal" , n , "n" , 1 )
    apifun_checkflint ( "makematrix_pascal" , k , "k" , 2 )
    //
    A = specfun_pascal ( n , k )
endfunction
