// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_rosser ( )
    //   Returns the Rosser matrix
    //
    // Calling Sequence
    //  A = makematrix_rosser ( )
    //
    // Parameters
    // A : a 8-by-8 matrix of doubles
    //
    // Description
    //   Returns the Rosser matrix.
    //
    // Examples
    // A = makematrix_rosser ( )
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //
    // Bibliography
    //  A handbook of numerical matrix inversion and solution of linear equations, Joan R. Westlake

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_toeplitz" , rhs , 0 : 0 )
    apifun_checklhs ( "makematrix_toeplitz" , lhs , 1 : 1 )
    //
    A = [
    611   196  -192   407    -8   -52   -49    29
    196   899   113  -192   -71   -43    -8   -44
    -192   113   899   196    61    49     8    52
    407  -192   196   611     8    44    59   -23
    -8   -71    61     8   411  -599   208   208
    -52   -43    49    44  -599   411   208   208
    -49    -8     8    59   208   208    99  -911
    29   -44    52   -23   208   208  -911    99
    ]
endfunction

