// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_toeplitz ( x )
    //   Returns the Toeplitz matrix
    //
    // Calling Sequence
    // A = makematrix_toeplitz ( x )
    //
    // Parameters
    //  x : a 1-by-n or n-by-1 matrix of doubles, the x input
    //  A : a n-by-n matrix of doubles, the Toeplitz matrix
    //
    // Description
    //   Returns the Toeplitz matrix of size n,
    //   made with entries from x.
	//
	//  A Toeplitz matrix has constant descending diagonals.
	//
	//   For example, consider n=5 and m=9.
	//   If <literal>x=[a b c d e f g h i]</literal>,
	//   then the associated 5-by-5 Toeplitz matrix is
	//  <screen>
	//  A = [
	//  a b c d e
	//  f a b c d
	//  g f a b c
	//  h g f a b
	//  i h g f a
	//  ]
	//  </screen>
	//
	// See makematrix_hankel for a similar matrix construction.
    //
    // Examples
	// A = makematrix_toeplitz ( 1:5 )
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //   http://en.wikipedia.org/wiki/Toeplitz_matrix
	//
	// See also
	// makematrix_circul
	// makematrix_hankel
    //
	
	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_toeplitz" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_toeplitz" , lhs , 1 : 1 )
	//
    if (x==[]) then
        A = []
        return
    end
	//
    apifun_checktype ( "makematrix_toeplitz" , x , "x" , 1 , "constant" )
	//
    apifun_checkvector ( "makematrix_toeplitz" , x , "x" , 1 )
	//
	x = x(:)'
    n = size(x,2)
    A = zeros(n,n)
    // Set the values over the diagonal
    first = 1
    last = n
    for i = 1:n
        A(i,i:n) = x(first:last)
        last = last - 1
    end
    // Set the values under the diagonal (strictly
    first = 2
    last = 2
    for i = 2:n
        A(i,1:i-1) = x(first:-1:last)
        first = first + 1
    end
endfunction

