// Copyright (C) 2011 - Michael Baudin
//  Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_wilkinsonp ( n )
    //   Returns the Wilkinson + matrix
    //
    // Calling Sequence
    //    A = makematrix_wilkinsonp ( n )
    //
    // Parameters
    //  n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    //  A : a n-by-n matrix of doubles, integer values
    //
    // Description
    //   "The W+matrix (Wilkinson 1965, p 308) is normally given odd order.
    //   This tridiagonal matrix then has several pairs of close eigenvalues
    //   despite the fact that no superdiagonal element is
    //   small. Wilkinson points out that the separation between the two largest eigenvalues
    //   is of the order of (n!)-2 so that the power method will be unable to
    //   separate them unless n is very small."
    //
    // Examples
    // A = makematrix_wilkinsonp ( 5 )
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  J.C. Nash, Compact Numerical Methods for Computers: Linear Algebra and Function Minimisation, second edition, Adam Hilger, Bristol, 1990 (Appendix 1).
    //  J.H. Wilkinson, The Algebraic Eigenvalue Problem, p. 308, "Close eigenvalues and small beta", W5+

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_wilkinsonp" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_wilkinsonp" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_wilkinsonp" , n , "n" , 1 , "constant" )
	//
    apifun_checkscalar ( "makematrix_wilkinsonp" , n , "n" , 1 )
	//
    apifun_checkflint ( "makematrix_wilkinsonp" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_wilkinsonp" , n , "n" , 1 , 1 )
    //
    A = zeros(n,n);
    for i = 1:n
        A(i,i) = int(n/2)+1-min(i, n- i+1)
    end
    for i = 1:n-1
        A(i,i+1) = 1.0
        A(i+1,i) = 1.0
    end
endfunction

