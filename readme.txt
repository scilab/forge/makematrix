Make Matrix toolbox

Purpose
-------

The goal of this toolbox is to provide a collection of
test matrices.

Some of these matrices appears in specific applied mathematics
problems.
For example, the Van Der Monde matrix appears in polynomial interpolation.
Another example is the Hilbert matrix, which arises in the least squares
approximation of arbitrary functions by polynomials.

These test matrices can also be used to experiment with linear algebra
algorithms, such as the resolution of systems of linear equations
or the eigenvalue problem.

For some of these test matrices, the exact eigenvalues, condition number or invert
is known.
For example, the Hilbert matrix is symetric positive definite.
Another example is Frank's matrix, which has a unit derminant, but
is ill-conditionned.

Features
-------

 * makematrix_border : Returns the Border matrix
 * makematrix_cauchy : Returns the Cauchy matrix
 * makematrix_circul : Returns the circular matrix
 * makematrix_diagonali : Returns a diagonal i matrix
 * makematrix_dingdong : Returns the Ding Dong matrix
 * makematrix_frank : Returns the Frank matrix
 * makematrix_frankmin : Returns the Frank-min matrix
 * makematrix_gallery : Returns a test matrix
 * makematrix_hadamard : Returns the Hadamard matrix
 * makematrix_hankel : Returns the Hankel matrix
 * makematrix_hilbert : Returns the Hilbert matrix
 * makematrix_invhilbert : Returns the inverse of the Hilbert matrix
 * makematrix_magic : Returns the magic matrix
 * makematrix_moler : Returns the Moler matrix
 * makematrix_normal : Returns a standard normal random matrix
 * makematrix_pascal : Returns the Pascal matrix
 * makematrix_rosser : Returns the Rosser matrix
 * makematrix_toeplitz : Returns the Toeplitz matrix
 * makematrix_vandermonde : Returns the Vandermonde matrix
 * makematrix_wilkinsonm : Returns the Wilkinson - matrix
 * makematrix_wilkinsonp : Returns the Wilkinson + matrix

Demos
-----

 * Vandermonde : Interpolation
 * Hilbert : Least Squares
 * Backslash Test


Dependencies
------------

 * This module depends on the helptbx module (to build the help pages).
 * This module depends on the assert module.
 * This module depends on the specfun module (pascal matrix).
 * This module depends on the apifun module (>=v0.2).

ATOMS
-------

http://atoms.scilab.org/toolboxes/makematrix

Authors
-------

 * Copyright (C) 2011 - Michael Baudin
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2008-2009 - INRIA - Michael Baudin

Licence
-------

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

TODO
-------
 * use apifun
 * Dorr matrix
 * Anti-Hadamard
 * Fiedler matrix
 * Extend Hadamard to multiples of 4


Bibliography
-------

 * "Compact Numerical Methods for Computers: Linear Algebra and Function Minimisation", second edition, J.C. Nash, Adam Hilger, Bristol, 1990 (Appendix 1).
 * "A handbook of numerical matrix inversion and solution of linear equations", Joan R. Westlake
 * "Accuracy and Stability of Numerical Algorithms", A Gallery of Test Matrices , Nicolas Higham
 * "Mathematical recreations and essays", 12th ed., W. W. Rouse Ball and H. S. M. Coxeter
 * "The Test Matrix Toolbox for Matlab (Version 3.0)", Nicolas Higham, Numerical Analysis Report No. 276, September 1995, Manchester Centre for Computational Mathematics, The University of Manchester
 * "Algorithm 694: a collection of test matrices in MATLAB", Nicolas Higham, ACM Transactions on Mathematical Software, Volume 17 , Issue 3 (September 1991), Pages: 289 - 305
 * "Higham's Test Matrices ", John Burkardt, 2005, http://people.sc.fsu.edu/~burkardt/m_src/test_matrix/test_matrix.html
 * "A Handbook of Numerical Matrix Inversion and Solution of Linear Equations", J.R. Westlake, John Wiley, New York, 1968.
 * "The Algebraic Eigenvalue Problem", J.H. Wilkinson, Oxford University Press, 1965.

