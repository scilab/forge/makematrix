// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->


computed = makematrix_dingdong ( 5 );
expected = [
0.1111111111111111 0.14285714285714285 0.20000000000000001 0.33333333333333331 1.0
0.14285714285714285 0.20000000000000001 0.33333333333333331 1.0 -1.0
0.20000000000000001 0.33333333333333331 1.0 -1.0 -0.33333333333333331
0.33333333333333331 1.0 -1.0 -0.33333333333333331 -0.20000000000000001
1.0 -1.0 -0.33333333333333331 -0.20000000000000001 -0.14285714285714285
];
assert_checkalmostequal ( computed , expected , 10 * %eps );

