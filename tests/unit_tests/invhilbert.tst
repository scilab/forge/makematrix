// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

computed = makematrix_invhilbert ( 5 );
expected = [
   25.     -300.      1050.     -1400.      630.
 -300.     4800.    -18900.     26880.   -12600.
 1050.   -18900.     79380.   -117600.    56700.
-1400.    26880.   -117600.    179200.   -88200.
  630.   -12600.     56700.    -88200.    44100.
];
assert_checkalmostequal ( computed , expected , 10 * %eps );

