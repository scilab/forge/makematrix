// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

computed = makematrix_vandermonde ( [] );
expected = [
];
assert_checkequal ( computed , expected );

computed = makematrix_vandermonde ( [1] );
expected = [
1
];
assert_checkequal ( computed , expected );

computed = makematrix_vandermonde ( 1:2 );
expected = [
1.     1.
1.     2.
];
assert_checkequal ( computed , expected );

computed = makematrix_vandermonde ( 1:3 );
expected = [
    1.    1.    1.
    1.    2.    4.
    1.    3.    9.
];
assert_checkequal ( computed , expected );

computed = makematrix_vandermonde ( 1:5 );
expected = [
    1.    1.    1.     1.      1.
    1.    2.    4.     8.      16.
    1.    3.    9.     27.     81.
    1.    4.    16.    64.     256.
    1.    5.    25.    125.    625.
];
assert_checkequal ( computed , expected );

// Use the n parameter
A = makematrix_vandermonde ( 1:5 );
B = makematrix_vandermonde ( 1:5 , 3 );
assert_checkequal(A(:,1:3),B);


// Use Vandermonde matrix to perform
// Lagrange interpolation.
// See http://en.wikipedia.org/wiki/Lagrange_polynomial
// The X points
x = [
-1.5
-0.75
0.
0.75
1.5
];
// The Y values
y = [
-14.1014
-0.931596
0.
0.931596
14.1014
];
// The matrix
V = makematrix_vandermonde(x);
// The coefficients of the interpolating
// polynomial
u = V\y;
expected = [
0.
-1.477474
0.
4.834848
0.
];
assert_checkalmostequal(u,expected,1.e-5,1.e-5);
// Compute values of the interpolating polynomial
xI = linspace(-2,2,1000);
n = size(x,"*");
V = makematrix_vandermonde(xI,n);
yI = V * u;
if ( %f ) then
// Draw the points
scf();
plot(x,y,"b*");
plot(xI,yI,"r-");
legend(["Data","Lagrange Polynomial"]);
xtitle("Lagrange Interpolation","X","Y");
end
